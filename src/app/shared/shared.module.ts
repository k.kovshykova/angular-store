import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { InputComponent } from './input/input.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input'



@NgModule({
  declarations: [
    ButtonComponent,
    InputComponent
  ],
  imports: [
    CommonModule, MatButtonModule, MatInputModule
  ],
  exports: [
    InputComponent, ButtonComponent
  ]
})
export class SharedModule { }
